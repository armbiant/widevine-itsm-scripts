from gtfs_kit import read_feed as read_gtfs
from json import loads
from pathlib import Path
from pytest import fixture
from itsim_scripts.utils import filter_gtfs, export_gtfs_to_geojson


@fixture
def data_path():
    return Path(__file__).resolve().parent / 'data'


@fixture
def nancy_gtfs_path(data_path):
    return data_path / 'nancy.zip'


def test_filter_gtfs(tmp_path, nancy_gtfs_path):
    routes = read_gtfs(nancy_gtfs_path, 'm').routes
    route_ids = sorted(routes.loc[routes['route_id'].astype('int').mod(2) == 0]['route_id'].to_list())
    route_ids_file = tmp_path / 'route.ids'
    with route_ids_file.open('w') as f:
        f.write('\n'.join(str(x) for x in route_ids))
    in_file = tmp_path / 'in.zip'
    out_file = tmp_path / 'out.zip'
    filter_gtfs(nancy_gtfs_path, route_ids_file, in_file, out_file)
    routes_in_ids = sorted(read_gtfs(in_file, 'm').routes['route_id'].to_list())
    routes_out_ids = sorted(read_gtfs(out_file, 'm').routes['route_id'].to_list())
    assert routes_in_ids == route_ids
    assert routes_out_ids != route_ids
    assert set(routes_out_ids) - set(routes_in_ids) == set(routes_out_ids)
    assert set(routes_in_ids) - set(routes_out_ids) == set(routes_in_ids)
    assert set(routes['route_id'].to_list()) - set(routes_in_ids) == set(routes_out_ids)


def test_export_gtfs_to_geojson(tmp_path, nancy_gtfs_path):
    json_file = tmp_path / 'nancy.json'
    export_gtfs_to_geojson(nancy_gtfs_path, json_file)
    with json_file.open() as f:
        geojson = loads(f.read().strip())
    assert isinstance(geojson, dict)
    assert 'type' in geojson
    assert geojson['type'] == 'FeatureCollection'
    assert 'features' in geojson
    features = geojson['features']
    assert isinstance(features, list)
    assert len(features) > 1000
    f101 = [f for f in features if f.get('properties', {}).get('route_id', None) == '101'][0]
    assert 'geometry' in f101
    del f101['geometry']
    assert f101 == {
        'type': 'Feature',
        'properties': {
            'route_id': '101',
            'route_short_name': '1',
            'route_long_name': "Vandoeuvre CHU Brabois - Essey Mouzimpré",
            'route_type': 0,
            'route_color': 'E0001A',
            'route_text_color': 'FFFFFF',
        },
    }
