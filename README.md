Itsim scripts library
=====================

How to use
----------

```
pip install itsim_scripts
```

Then in python
```
from itsim_scripts import utils
```

How to develop on it
--------------------

```
pip install -e .
```
